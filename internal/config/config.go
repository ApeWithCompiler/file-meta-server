package config

import (
    "gopkg.in/yaml.v2"
    "io/ioutil"
    "log"
)

type Config struct {
    ListenAddr string `yaml:"listenAddr"`
    MongoUri string `yaml:"mongoUri"`
    MongoDatabase string `yaml:"mongoDatabase"`
}

func GetDefaultFile() string {
    return "config.yml"
}

func GetConfig(file string) Config {
    yamlFile, err := ioutil.ReadFile(file)
    if err != nil {
        log.Printf("yamlFile.Get err   #%v ", err)
    }

    var conf Config
    err = yaml.Unmarshal(yamlFile, &conf)
    if err != nil {
        log.Fatalf("Unmarshal: %v", err)
    }

    return conf
}