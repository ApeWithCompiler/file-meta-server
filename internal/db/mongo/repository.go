package mongo

import (
    "log"
    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Repository struct {
    dbi DBInterface
    collectionName string
}

func (r *Repository) insertOne(obj interface{}) string{
	database, ctx := r.dbi.GetDatabase()
    collection := database.Collection(r.collectionName)

    res, err := collection.InsertOne(ctx, obj)
    if err != nil {
        log.Fatal(err)
    }

    return res.InsertedID.(primitive.ObjectID).Hex()
}

func (r *Repository) updateOne(filter interface{}, update interface{}){
    database, ctx := r.dbi.GetDatabase()
    collection := database.Collection(r.collectionName)

    _, err := collection.UpdateOne(ctx, filter, update)
    if err != nil {
        log.Fatal(err)
    }
}

func (r *Repository) getOne(objectId primitive.ObjectID, out interface{}){
	database, ctx := r.dbi.GetDatabase()
    collection := database.Collection(r.collectionName)

    cursor := collection.FindOne(ctx, bson.M{"_id": objectId})

    err := cursor.Decode(out)
    if err != nil {
        log.Fatal(err)
    }
}

func (r *Repository) exists(filter bson.M, out interface{}) bool{
    database, ctx := r.dbi.GetDatabase()
    collection := database.Collection(r.collectionName)

    cursor := collection.FindOne(ctx, filter)

    err := cursor.Decode(out)
    if err != nil {
        if err == mongo.ErrNoDocuments {
            return false
        }
        log.Fatal(err)
    }

    return true
}


func (r *Repository) getAll(out interface{}){
	database, ctx := r.dbi.GetDatabase()
    collection := database.Collection(r.collectionName)

    cursor, cursorErr := collection.Find(ctx, bson.M{})

    if cursorErr != nil { panic(cursorErr) }
    defer cursor.Close(ctx)

    if err := cursor.All(ctx, out); err != nil {
          panic(err)
    }
}

func (r *Repository) deleteOne(objectId primitive.ObjectID){
    database, ctx := r.dbi.GetDatabase()
    collection := database.Collection(r.collectionName)

    _, err := collection.DeleteOne(ctx, bson.M{"_id": objectId})
    if err != nil {
        log.Fatal(err)
    }

}

func (r *Repository) find(query bson.M, out interface{}){
    database, ctx := r.dbi.GetDatabase()
    collection := database.Collection(r.collectionName)

    cursor, cursorErr := collection.Find(ctx, query)

    if cursorErr != nil { panic(cursorErr) }
    defer cursor.Close(ctx)

    if err := cursor.All(ctx, out); err != nil {
          panic(err)
    }
}