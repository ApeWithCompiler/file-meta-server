package mongo

import (
    "go.mongodb.org/mongo-driver/bson"
    "go.mongodb.org/mongo-driver/bson/primitive"
    "gitlab.com/apewithcompiler/file-meta-server/internal/model"
)

const documentTable = "files"

type FileReaderWriter struct {
    Repository
}

func NewFileReaderWriter(DBI DBInterface) FileReaderWriter {
    return FileReaderWriter{
        Repository{
            dbi: DBI,
            collectionName: documentTable,
        },
    }
}

func (fileRW *FileReaderWriter) Create(newObject model.File) string{
    insertId := fileRW.insertOne(newObject)

    return insertId
}

func (fileRW *FileReaderWriter) Get(id string) model.File{
    var file model.File
    objectId, _ := primitive.ObjectIDFromHex(id)
    fileRW.getOne(objectId, &file)
    return file
}

func (fileRW *FileReaderWriter) Delete(id string) {
    objectId, _ := primitive.ObjectIDFromHex(id)
    fileRW.deleteOne(objectId)
}

func (fileRW *FileReaderWriter) GetAll() []model.File{
    var file []model.File

    fileRW.getAll(&file)
    return file
}

func (fileRW *FileReaderWriter) UpdateLabel(id string, labels []model.Label){
    objectId, _ := primitive.ObjectIDFromHex(id)
    filter := bson.M{"_id": objectId}
    update := bson.M{"$set": bson.M{"labels": labels}}
    fileRW.updateOne(filter, update)
}

func (fileRW *FileReaderWriter) IdExists(id string) bool{
    objectId, _ := primitive.ObjectIDFromHex(id)
    filter := bson.M{"_id": objectId}
    return fileRW.exists(filter, &model.File{})
}

func (fileRW *FileReaderWriter) PathExists(path string) bool{
    filter := bson.M{"path": path}
    return fileRW.exists(filter, &model.File{})
}

func (fileRW *FileReaderWriter) FindByPath(path string) []model.File{
    var file []model.File

    filter := bson.M{"path": path}
    fileRW.find(filter, &file)
    return file
}

func (fileRW *FileReaderWriter) FindByPathRegex(path string) []model.File{
    var file []model.File

    filter := bson.M{"path": bson.D{{"$regex", primitive.Regex{Pattern: path}}}}
    fileRW.find(filter, &file)
    return file
}

func (fileRW *FileReaderWriter) FindByLabel(label model.Label) []model.File{
    var file []model.File

    filter := bson.M{}
    if label.Namespace != "" {
        filter["labels.namespace"] = label.Namespace
    }
    if label.Name != "" {
        filter["labels.name"] = label.Name
    }
    if label.Value != "" {
        filter["labels.value"] = label.Value
    }

    fileRW.find(filter, &file)
    return file
}