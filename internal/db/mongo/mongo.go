package mongo

import (
    "context"
    "log"
    "time"

    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
)

type DBInterface struct {
    client *mongo.Client
    ctx context.Context
    databaseName string
}

func NewDBInterface(MongoURI, DatabaseName string) DBInterface {
    client, err := mongo.NewClient(options.Client().ApplyURI(MongoURI))
    if err != nil {
        log.Fatal(err)
    }
    timeCtx, _ := context.WithTimeout(context.Background(), 10*time.Second)

    return DBInterface{
        client: client,
        ctx: timeCtx,
        databaseName: DatabaseName,
    }
}

func (dbi *DBInterface) Connect() {
    err := dbi.client.Connect(dbi.ctx)
    if err != nil {
        log.Fatal(err)
    }
}

func (dbi *DBInterface) Disconnect() {
    err := dbi.client.Disconnect(dbi.ctx)
    if err != nil {
        log.Fatal(err)
    }
}

func (dbi *DBInterface) GetDatabase() (*mongo.Database, context.Context){
    return dbi.client.Database(dbi.databaseName), dbi.ctx
}