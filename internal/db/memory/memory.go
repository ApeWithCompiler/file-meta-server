package memory

import (
	"sync"
	
	"gitlab.com/apewithcompiler/file-meta-server/internal/model"
)

type MemoryDB struct {
	documents map[string]model.Document
}

var lock = &sync.Mutex{}
var (
	instance *MemoryDB
)

func CreateDB() *MemoryDB{
	lock.Lock()
	defer lock.Unlock()

	if instance == nil {
		instance = &MemoryDB{
			documents: make(map[string]model.Document),
		} // <-- thread safe
	}

	return instance
}

func (db *MemoryDB) Create(newObj model.Document) {
	db.documents[newObj.ID] = newObj
}

func (db *MemoryDB) Get(modelId string) model.Document{
	return db.documents[modelId]
}

func (db *MemoryDB) Delete(modelId string){
	delete(db.documents, modelId)
}


func (db *MemoryDB) List() []model.Document{
	var res []model.Document
	for _, element := range db.documents {
		res = append(res, element)
	}
	return res
}