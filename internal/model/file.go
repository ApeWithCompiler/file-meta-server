package model

import (
)

type Label struct {
	Namespace string `bson:"namespace"`
	Name string `bson:"name"`
	Value string `bson:"value"`
}

type File struct {
	ID string `bson:"_id,omitempty"`
	Path string `bson:"path,omitempty"`
	Labels []Label `bson:"labels,omitempty"`
}
