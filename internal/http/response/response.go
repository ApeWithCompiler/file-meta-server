package response

import (
)

type DataResponse struct {
    Links Links `json:"links"`
    Data []interface{} `json:"data"`
}

type Links struct {
    Self string `json:"self"`
    Related string `json:"related"`
}