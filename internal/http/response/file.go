package response

import (
    "net/http"
    "gitlab.com/apewithcompiler/file-meta-server/internal/model"
)

type Label struct {
    Namespace string `json:"namespace"`
    Name string `json:"name"`
    Value string `json:"value"`
}

type File struct {
    ID string `bson:"id"`
    Path string `json:"path"`
    Labels []Label `json:"labels"`
    Links Links `json:"links"`
}

func ParseLabelFromModel(labelModel model.Label) Label {
    return Label {
        Namespace: labelModel.Namespace,
        Name: labelModel.Name,
        Value: labelModel.Value,
    }  
}


func ParseFileFromModel(fileModel model.File) File {
    file := File {
        ID: fileModel.ID,
        Path: fileModel.Path,
        Links: Links {
            Self: "/files/" + fileModel.ID,
        },
    }

    for _, label := range fileModel.Labels {
        file.Labels = append(file.Labels, ParseLabelFromModel(label))
    }

    return file
}

func ParseFileResponseFromModelSlice(fileModelSlice []model.File) DataResponse {
    resp := DataResponse {
        Links: Links {
            Self: "/files",
        },
    }

    for _, file := range fileModelSlice {
        resp.Data = append(resp.Data, ParseFileFromModel(file))
    }

    return resp     
}

func ParseErrorFileExistsResponseFromPathSlice(r *http.Request, paths []string) ErrorResponseBody {
    var resp ErrorResponseBody

    for _, path := range paths {
        err := ErrorResponseObject{
            Status: "400",
            Code: "400",
            Title: "File allready exists",
            Detail: "The file with the provides path allready is existing in the database",
            Source: ErrorResponseSource {
                Pointer: r.RequestURI,
                Parameter: path,
            },
        }

        resp.Errors = append(resp.Errors, err)
    }

    return resp    
}