package request

import (
    "gitlab.com/apewithcompiler/file-meta-server/internal/model"    
)

type Label struct {
    Namespace string `json:"namespace"`
    Name string `json:"name"`
    Value string `json:"value"`
}

type File struct {
    Path string `json:"path"`
    Labels []Label `json:"labels"`
}


type FileRequest struct {
    Data []File `json:"data"`
}

type LabelRequest struct {
    Data []Label `json:"data"`
}

func ParseLabelModelFromRequest(reqLabel Label) model.Label {
    return model.Label {
        Namespace: reqLabel.Namespace,
        Name: reqLabel.Name,
        Value: reqLabel.Value,
    }
}

func ParseFileModelFromRequest(reqFile File) model.File {
    file := model.File {
        Path: reqFile.Path,
    }

    for _, label := range reqFile.Labels {
        file.Labels = append(file.Labels, ParseLabelModelFromRequest(label))        
    }

    return file
}