package route

import (
    "os"
    "log"
    "net/http"
    
    "github.com/gorilla/mux"
    "github.com/gorilla/handlers"

    "gitlab.com/apewithcompiler/file-meta-server/internal/config"
    "gitlab.com/apewithcompiler/file-meta-server/internal/http/handler"
)

func HandleRequest() {
    conf := config.GetConfig(config.GetDefaultFile())

    router := mux.NewRouter()

    router.Handle("/files", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(handler.AddFileHandler))).Methods("POST")
    router.Handle("/files", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(handler.ListFilesHandler))).Methods("GET")
    router.Handle("/files/{fileId}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(handler.GetFileHandler))).Methods("GET")
    router.Handle("/files/{fileId}", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(handler.DeleteFileHandler))).Methods("DELETE")
    router.Handle("/files/{fileId}/labels", handlers.LoggingHandler(os.Stdout, http.HandlerFunc(handler.UpdateLabelFileHandler))).Methods("PUT")

    log.Fatal(http.ListenAndServe(conf.ListenAddr, router))
}
