package handler

import (
    "net/http"
    "io/ioutil"
    "encoding/json"
    
    "github.com/gorilla/mux"

    "gitlab.com/apewithcompiler/file-meta-server/internal/config"
    "gitlab.com/apewithcompiler/file-meta-server/internal/model"
    "gitlab.com/apewithcompiler/file-meta-server/internal/db/mongo"
    "gitlab.com/apewithcompiler/file-meta-server/internal/http/request"
    "gitlab.com/apewithcompiler/file-meta-server/internal/http/response"
)

func AddFileHandler(w http.ResponseWriter, r *http.Request){
    requestBodyBlob, _ := ioutil.ReadAll(r.Body)
    var requestBody request.FileRequest
    json.Unmarshal(requestBodyBlob, &requestBody)

    conf := config.GetConfig(config.GetDefaultFile())
    db := mongo.NewDBInterface(conf.MongoUri, conf.MongoDatabase)
    db.Connect()
    defer db.Disconnect()
    readerwriter := mongo.NewFileReaderWriter(db)

    var duplicatePaths []string
    for _, reqFile := range requestBody.Data {
        if readerwriter.PathExists(reqFile.Path) {
            duplicatePaths = append(duplicatePaths, reqFile.Path)
        }
    }

    if len(duplicatePaths) != 0 {
        resp := response.ParseErrorFileExistsResponseFromPathSlice(r, duplicatePaths)
        w.Header().Add("Content-Type", "application/json")
        w.WriteHeader(http.StatusBadRequest)
        json.NewEncoder(w).Encode(resp)
        return        
    }

    var files []model.File
    for _, reqFile := range requestBody.Data {
        file := request.ParseFileModelFromRequest(reqFile)

        insertedId := readerwriter.Create(file)
        insertedFile := readerwriter.Get(insertedId)
        files = append(files, insertedFile)
    }
 
    resp := response.ParseFileResponseFromModelSlice(files)
    w.Header().Add("Content-Type", "application/json")
    json.NewEncoder(w).Encode(resp)   
}

func ListFilesHandler(w http.ResponseWriter, r *http.Request){
    queryLabelNamespace := r.FormValue("label.namespace")
    queryLabelName := r.FormValue("label.name")
    queryLabelValue := r.FormValue("label.value")
    queryPath := r.FormValue("path")
    queryPathRegex := r.FormValue("path.regex")
    //queryLimit := r.FormValue("limit")
    //queryPage := r.FormValue("page")

    conf := config.GetConfig(config.GetDefaultFile())
    db := mongo.NewDBInterface(conf.MongoUri, conf.MongoDatabase)
    db.Connect()
    defer db.Disconnect()
    readerwriter := mongo.NewFileReaderWriter(db)

    var files []model.File

    if queryPath != "" {
        if queryPathRegex != "true" {
            files = readerwriter.FindByPath(queryPath)
        } else {
            files = readerwriter.FindByPathRegex(queryPath)
        }
    } else if (queryLabelNamespace != "" || queryLabelName != "" || queryLabelValue != "") {
        filter := model.Label{
            Namespace: queryLabelNamespace,
            Name: queryLabelName,
            Value: queryLabelValue,
        }

        files = readerwriter.FindByLabel(filter)
    } else {
        files = readerwriter.GetAll()
    }

    resp := response.ParseFileResponseFromModelSlice(files)
    w.Header().Add("Content-Type", "application/json")
    json.NewEncoder(w).Encode(resp)
}

func GetFileHandler(w http.ResponseWriter, r *http.Request){
    parameters := mux.Vars(r)
    fileId, _ := parameters["fileId"]

    conf := config.GetConfig(config.GetDefaultFile())
    db := mongo.NewDBInterface(conf.MongoUri, conf.MongoDatabase)
    db.Connect()
    defer db.Disconnect()
    readerwriter := mongo.NewFileReaderWriter(db)

    if !readerwriter.IdExists(fileId) {
        resp := response.ParseErrorNotFound(r, fileId)
        w.Header().Add("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotFound)
        json.NewEncoder(w).Encode(resp)
        return        
    }

    file := readerwriter.Get(fileId)
    files := []model.File{file} 
    
    resp := response.ParseFileResponseFromModelSlice(files)
    w.Header().Add("Content-Type", "application/json")
    json.NewEncoder(w).Encode(resp)
}

func DeleteFileHandler(w http.ResponseWriter, r *http.Request){
    parameters := mux.Vars(r)
    fileId, _ := parameters["fileId"]

    conf := config.GetConfig(config.GetDefaultFile())
    db := mongo.NewDBInterface(conf.MongoUri, conf.MongoDatabase)
    db.Connect()
    defer db.Disconnect()
    readerwriter := mongo.NewFileReaderWriter(db)

    if !readerwriter.IdExists(fileId) {
        resp := response.ParseErrorNotFound(r, fileId)
        w.Header().Add("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotFound)
        json.NewEncoder(w).Encode(resp)
        return        
    }

    file := readerwriter.Get(fileId)
    files := []model.File{file} 

    readerwriter.Delete(fileId)

    resp := response.ParseFileResponseFromModelSlice(files)
    w.Header().Add("Content-Type", "application/json")
    json.NewEncoder(w).Encode(resp)
}

func UpdateLabelFileHandler(w http.ResponseWriter, r *http.Request){
    parameters := mux.Vars(r)
    fileId, _ := parameters["fileId"]

    requestBodyBlob, _ := ioutil.ReadAll(r.Body)
    var requestBody request.LabelRequest
    json.Unmarshal(requestBodyBlob, &requestBody)

    conf := config.GetConfig(config.GetDefaultFile())
    db := mongo.NewDBInterface(conf.MongoUri, conf.MongoDatabase)
    db.Connect()
    defer db.Disconnect()
    readerwriter := mongo.NewFileReaderWriter(db)

    if !readerwriter.IdExists(fileId) {
        resp := response.ParseErrorNotFound(r, fileId)
        w.Header().Add("Content-Type", "application/json")
        w.WriteHeader(http.StatusNotFound)
        json.NewEncoder(w).Encode(resp)
        return        
    }

    var labels []model.Label
    for _, reqLabel := range requestBody.Data {
        labels = append(labels, request.ParseLabelModelFromRequest(reqLabel))
    }

    readerwriter.UpdateLabel(fileId, labels)

    file := readerwriter.Get(fileId)
    files := []model.File{file} 

    resp := response.ParseFileResponseFromModelSlice(files)
    w.Header().Add("Content-Type", "application/json")
    json.NewEncoder(w).Encode(resp)
}