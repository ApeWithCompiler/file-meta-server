module gitlab.com/apewithcompiler/file-meta-server

go 1.16

require (
    github.com/gorilla/mux v1.8.0
    github.com/gorilla/handlers v1.5.1
    gopkg.in/yaml.v2 v2
)