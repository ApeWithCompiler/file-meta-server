package main

import (
    "gitlab.com/apewithcompiler/file-meta-server/internal/http/route"
)

func main() {
	route.HandleRequest()
}